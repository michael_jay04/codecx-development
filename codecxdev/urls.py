from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'codecxdev.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    #url('^categories/', include('codecx.url.category')),
    url(r'^tinymce/', include('tinymce.urls')),
    url(r'^admin/', include(admin.site.urls)),
)
