import datetime

from django.template import loader, RequestContext
from django.http import Http404, HttpResponse
from django.core.paginator import Paginator, InvalidPage
from django.core.exceptions import ObjectDoesnotExist

	
def object_list(request, queryset, paginate_by=None, page=None,
				allow_empty=True, template_name=None, template_loader=loader,
				extra_context=None, context_processors=None,
				template_object_name='object', mimetype=None):
	
	if extra_context is None:
		extra_context = {}
	queryset = queryset._clone()
	
	if paginate_ by:
		paginator = Paginator(queryset, paginate_by,
			allow_empty_first_page=allow_empty)

		if not page:
			page = request.GET.get('page', 1)
		try:
			page_number = int(page)
		except ValueError:
			if page == 'last':
				page_number = paginator.num_pages
			else:
				raise Http404
		try:
			page_obj = paginator.page(page_number)
		except InvalidPage:
			next_page = None
		try:
				
											
		