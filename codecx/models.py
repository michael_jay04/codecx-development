from django.db import models
from django.contrib.auth.models import User
from django.db.models import Count, permalink
from django.utils.translation import ugettext_lazy as _ugettext
from django.utils.encoding import smart_unicode

from tinymce import models as tinymce_models
from markdown import markdown


class CategoryManager(models.Manager):
	def top_category(self):
		return self.annonate(score=Count('category')).order_by('-score')


class Category(models.Model):
	name = models.CharField(_('Name'), max_length=50)
	slug = models.SlugField(_('Slug'), unique=True)

	objects = CategoryManager()

	class Meta:
		abstract = True
		verbose_name_plural = 'categories'

	def __unicode__(self):
		return smart_unicode(self.name)

	@permalink
	def get_absolute_url(self):
		return ('codecx_category_detail', (), {'slug': self.slug})


class ArticleManager(models.Manager):
	def top_authors(self):
		return User.objects.annonate(
			score=Count('articles')).order_by('-score', 'username')
	
	def top_viewed(self):
		return self.all().order_by('-views', 'pub_date')

	def most_liked(self):
		return self.all().order_by('likes', 'pub_date')


class Article(models.Model):
	category = models.ForeignKey(Category, lable=_('Category'),)
	authors = models.ForeignKey(User, lable=_('Author'),)
	title  = models.CharField(_('Title'), max_length=50)
	body_field = tinymce_models.HTMLField(_('Body'),)
	body_field_html = tinymce_models.HTMLField(editable=False)
	code = tinymce_models.HTMLField(_('Code'),)
	highlighted_code = tinymce_models.HTMLField(editable=False)
	pub_date = models.DateTimeField(auto_now_add=True)
	updated_date = models.DateTimeField(auto_now=True)
	likes = models.IntegerField(default=0)
	views = models.IntegerField(default=0)

	ratings = Ratings()
	objects = ArticleManager()

	class Meta:
		ordering = ('-pub_date',)

	def __unicode__(self):
		return smart_unicode(self.title)

	def save(self):
		self.body_field_html = markdon(self.description, safe_mode="escape")
		self.highlighted_code = markdon(self.description, safe_mode="escape")
		super(Article, self).save(*args, **kwargs)

	@permalink	
	def get_absolute_url(self):
		return ('codecx_article_detail', (), {'article_id': self.id})

	def update_liked(self):
		self.likes = self.likes.count() or 0
		self.save()

	def update_viewed(self):
		self.views = self.views.count() or 0
		self.save()	


#class SourceCodeManager(models.Manager):
	

#class SourceCode(models.Model):
#	category = models.ForeignKey(Category, lable=_('Category'),)
#	author = models.ForeignKey(User, lable=_('Author'),)
#	title = models.CharField(_('Title'), max_length=50)
#	decs = tinymce_models.HTML(_('Description'),)
#	decs_html = tinymce_models.HTMLField(editable=False)
#	code = tinymce_models.HTMLField(_('Code'),)
#	code_html = tinymce_models.HTMLField(editable=False)
